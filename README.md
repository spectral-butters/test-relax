# Slot Game

Welcome to the Slot Game project!

This project aims to provide a basic implementation of a dice and slot machine games.

It includes a service for handling game mechanics and a test suite to ensure the correctness of the implemented logic.

Application is console based, using `Java 16, Spring boot and Maven`.

## Installation
1. Clone the repository
2. Run `mvn clean install` to run tests and build the project
3. Run `mvn spring-boot:run` to start the application
4. Application will simulate 3 games based on configs in `application.yml` file and print out the results.

## Usage
1. Application has 3 configs: 2 for dice game and 1 for slot machine game.
2. Feel free to amend the configs in `application.yml` file.
3. Starting point of the application is `GamePlayService.test()` function.