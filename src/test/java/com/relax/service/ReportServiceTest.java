package com.relax.service;


import com.relax.entity.dice.DiceGame;
import com.relax.entity.Game;
import com.relax.entity.Round;
import com.relax.config.TestData;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReportServiceTest {

    ReportService gameplayService;

    @BeforeEach
    void setUp() {
        gameplayService = new ReportService();
    }

    @Test
    void testCalculateMean() {
        // Setup
        final Game game = new DiceGame(null);

        // Add 3 win round with win amount of 1, 2, 3
        for (int i = 0; i < 3; i++) {
            Round round = TestData.getWinRound(game);
            round.setWin(BigDecimal.valueOf(i + 1));
            game.getRounds().put(round.getId(), round);
        }

        // Add 3 loss round with win amount of 0
        for (int i = 0; i < 3; i++) {
            Round round = TestData.getLossRound(game);
            game.getRounds().put(round.getId(), round);
        }

        // Mean = sqrt(14 / 6) = 1.526

        // Run the test
        final BigDecimal result = gameplayService.calculateMean(game);

        // Verify the results
        assertEquals(new BigDecimal("1.53"), result);
    }

    @Test
    void testCalculateArithmeticalMean() {
        // Setup
        final Game game = new DiceGame(null);

        // Add 3 win round with win amount of 2, 3, 4
        for (int i = 0; i < 3; i++) {
            Round round = TestData.getWinRound(game);
            round.setWin(BigDecimal.valueOf(i + 2));
            game.getRounds().put(round.getId(), round);
        }

        // Add 3 loss round with win amount of 0
        for (int i = 0; i < 3; i++) {
            Round round = TestData.getLossRound(game);
            game.getRounds().put(round.getId(), round);
        }

        // Mean = (2 + 3 + 4 / 6) = 1.5

        // Run the test
        final BigDecimal result = gameplayService.calculateArithmeticalMean(game);

        // Verify the results
        assertEquals(new BigDecimal("1.50"), result);
    }

    @Test
    void testCalculateStandardDeviation() {
        // Setup
        final Game game = new DiceGame(null);

        Round round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(2));
        game.getRounds().put(round.getId(), round);

        round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(3));
        game.getRounds().put(round.getId(), round);

        round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(7));
        game.getRounds().put(round.getId(), round);

        round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(8));
        game.getRounds().put(round.getId(), round);

        // Wins 2, 3, 7, 8
        // Total rounds = 4

        // Run the test
        final BigDecimal result = gameplayService.calculateStandardDeviation(game);

        // Verify the results
        assertEquals(new BigDecimal("2.55"), result);
    }

    @Test
    void testCalculateVariance() {
        // Setup
        final Game game = new DiceGame(null);

        Round round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(2));
        game.getRounds().put(round.getId(), round);

        round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(3));
        game.getRounds().put(round.getId(), round);

        round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(7));
        game.getRounds().put(round.getId(), round);

        round = TestData.getWinRound(game);
        round.setWin(BigDecimal.valueOf(8));
        game.getRounds().put(round.getId(), round);

        // Wins 2, 3, 7, 8
        // Total rounds = 4
        // Standard deviation = sqrt(14 / 6) = 2.55
        // Variance = 2.55^2 = 6.5025

        // Run the test
        final BigDecimal result = gameplayService.calculateVariance(game);

        // Verify the results
        assertEquals(new BigDecimal("6.50"), result);
    }



}
