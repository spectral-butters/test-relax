package com.relax.service;

import com.relax.entity.slot.*;
import com.relax.config.SlotConfig;
import com.relax.config.TestData;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.relax.entity.slot.SymbolType.*;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class SlotGameServiceTest {

    SlotGameService slotGameService;

    @BeforeEach
    void setUp() {
        slotGameService = new SlotGameService();
    }

    @Test
    void testCalculation() {
        int rows = 3;
        int middle = 1;
        int j = 1;
        int prevIndex = (middle - j + rows) % rows;
        int nextIndex = (middle + j) % rows;

        assertEquals(0, prevIndex);
        assertEquals(2, nextIndex);

        j = 2;
        prevIndex = (middle - j + rows) % rows;
        nextIndex = (middle + j) % rows;

        assertEquals(2, prevIndex);
        assertEquals(0, nextIndex);

        j = 3;
        prevIndex = (middle - j + rows) % rows;
        nextIndex = (middle + j) % rows;

        assertEquals(1, prevIndex);
        assertEquals(1, nextIndex);
    }

    @Test
    void test_Next_Previous_Symbols() {
        SlotConfig.ReelConfig config = new SlotConfig.ReelConfig();
        config.setReels(new ArrayList<>());
        config.getReels().add(List.of(L1, SymbolType.H1, SymbolType.W1, SymbolType.L4, L2, L3));

        Reel reel = new Reel(config.getReels().get(0));
        Symbol symbol = reel.getSymbols().get(0);

        // Stepping back by 1, fetching from the end of the list
        int j = 1;
        Symbol previousSymbol1 =  reel.getPrevious(symbol.getNum(), j);
        assertEquals(reel.getSymbols().get(reel.getSymbols().size() - 1), previousSymbol1);

        // Stepping back by 2, fetching from the end of the list
        j = 2;
        Symbol previousSymbol2 =  reel.getPrevious(symbol.getNum(), j);
        assertEquals(reel.getSymbols().get(reel.getSymbols().size() - 2), previousSymbol2);

        //Stepping forward by 1 from the last, fetching from the beginning of the list
        j = 1;
        symbol = reel.getSymbols().get(reel.getSymbols().size() - 1);
        Symbol nextSymbol1 = reel.getNext(symbol.getNum(), j);
        assertEquals(reel.getSymbols().get(0), nextSymbol1);

        //Stepping forward by 2, fetching from the beginning of the list
        j = 2;
        Symbol nextSymbol2 = reel.getNext(symbol.getNum(), j);
        assertEquals(reel.getSymbols().get(1), nextSymbol2);
    }

    @Test
    void testParsePayline() {
        // Create a sample SlotGame object
        SlotConfig slotConfig = TestData.getSlotConfig();

        SlotGame game = new SlotGame(slotConfig);

        // Considering we have a 3x3 grid
        // L1 L2 L3
        // L1 L1 L1
        // L2 L2 L1
        // And a payline of 10, 11, 12

        Symbol[][] symbols = {
                {new Symbol(0, L1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, L2), new Symbol(2, L2), new Symbol(2, SymbolType.L1)}
        };

        assertEquals(symbols[1][0].getType(), L1);
        assertEquals(symbols[1][1].getType(), L1);
        assertEquals(symbols[1][2].getType(), L1);

        // Set up a sample payline configuration
        Payline payline1 = new Payline(List.of("10", "11", "12"));

        game.getPaylines().add(payline1);

        // Test the parsePayline method
        List<SymbolType> winSymbols = slotGameService.checkPaylines(game, symbols);

        // Verify the expected win symbols
        assertEquals(1, winSymbols.size());
        assertEquals(winSymbols.get(0), L1);

        // Setup another payline diagonally 00 11 22
        Payline payline2 = new Payline(List.of("00", "11", "22"));
        game.getPaylines().add(payline2);

        winSymbols = slotGameService.checkPaylines(game, symbols);
        assertEquals(2, winSymbols.size());
        assertEquals(winSymbols.get(0), L1);
        assertEquals(winSymbols.get(1), L1);

        // Test a payline with a wild card
        symbols = new Symbol[][]{
                {new Symbol(0, W1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, L2), new Symbol(2, L2), new Symbol(2, SymbolType.L1)}
        };
        game.setPaylines(new ArrayList<>());
        // Keep 1 payline diagonally 00 11 22
        game.getPaylines().add(payline2);

        // W1 must replace L1 during the payline check
        winSymbols = slotGameService.checkPaylines(game, symbols);
        assertEquals(1, winSymbols.size());
        // Symbol type must be L1
        assertEquals(winSymbols.get(0), L1);

        // Check with 2 W1 symbols
        symbols = new Symbol[][]{
                {new Symbol(0, W1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, L2), new Symbol(2, L2), new Symbol(2, SymbolType.W1)}
        };

        // W1 must replace L1 during the payline check
        winSymbols = slotGameService.checkPaylines(game, symbols);
        assertEquals(1, winSymbols.size());
        // Symbol type must be L1
        assertEquals(winSymbols.get(0), L1);

        // Check with 3 W1 symbols
        symbols = new Symbol[][]{
                {new Symbol(0, W1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.W1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, L2), new Symbol(2, L2), new Symbol(2, SymbolType.W1)}
        };

        // W1 must be the winning symbol
        winSymbols = slotGameService.checkPaylines(game, symbols);
        assertEquals(1, winSymbols.size());
        // Symbol type must be L1
        assertEquals(winSymbols.get(0), W1);

        // Check with W1 and B1 symbols
        symbols = new Symbol[][]{
                {new Symbol(0, W1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.B1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, L2), new Symbol(2, L2), new Symbol(2, SymbolType.W1)}
        };

        // W1 must not replace bonus symbol
        winSymbols = slotGameService.checkPaylines(game, symbols);
        assertEquals(0, winSymbols.size());

        // Check with W1 and other symbols
        symbols = new Symbol[][]{
                {new Symbol(0, W1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, L1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, L2), new Symbol(2, L2), new Symbol(2, L2)}
        };

        // No match must be found
        winSymbols = slotGameService.checkPaylines(game, symbols);
        assertEquals(0, winSymbols.size());

        // Validate triggerBonus function
        assertFalse(slotGameService.triggerBonus(symbols, 3));

        symbols = new Symbol[][]{
                {new Symbol(0, B1), new Symbol(0, L2), new Symbol(0, L3)},
                {new Symbol(1, SymbolType.L1), new Symbol(1, SymbolType.B1), new Symbol(1, SymbolType.L1)},
                {new Symbol(2, B1), new Symbol(2, L2), new Symbol(2, SymbolType.W1)}
        };

        // Validate triggerBonus function
        assertTrue(slotGameService.triggerBonus(symbols, 3));

    }

    @Test
    void testCalculateWin() {
        List<SymbolType> symbolTypes = List.of(L1, L2, H1);
        BigDecimal win = L1.getPayout().add(L2.getPayout()).add(H1.getPayout());
        assertEquals(win, slotGameService.calculateWin(symbolTypes));

        assertEquals(BigDecimal.ZERO, slotGameService.calculateWin(new ArrayList<>()));
    }

    @Test
    void testSpin() {
        // Create a sample SlotGame object
        // Considering 3x3 grid from the config
        SlotConfig slotConfig = TestData.getSlotConfig();

        SlotGame game = new SlotGame(slotConfig);
        game.initialize(slotConfig);

        Symbol[][] symbols = slotGameService.spin(game);
        assertNotNull(symbols[0][0]);
        assertNotNull(symbols[0][1]);
        assertNotNull(symbols[0][2]);
        assertNotNull(symbols[1][0]);
        assertNotNull(symbols[1][1]);
        assertNotNull(symbols[1][2]);
        assertNotNull(symbols[2][0]);
        assertNotNull(symbols[2][1]);
        assertNotNull(symbols[2][2]);

    }
}
