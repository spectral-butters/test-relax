package com.relax.service;

import com.relax.entity.Game;
import com.relax.entity.report.GameResult;
import com.relax.config.DiceTask1Config;
import com.relax.config.DiceTask2Config;
import com.relax.config.TestData;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class DiceGameServiceTest {

    DiceGameService diceGameService;
    ReportService reportService;

    @BeforeEach
    void setUp() {
        diceGameService = new DiceGameService();
        reportService = new ReportService();
    }

    @Test
    public void testDiceGame_task1() {
        // Setup
        final DiceTask1Config config = TestData.getDiceConfigTask1();

        // Run the test
        Game game = diceGameService.playDiceGame(config);

        BigDecimal expTotalBet = config.getBetAmount().multiply(BigDecimal.valueOf(config.getRoundCount()));

        // Verify the results
        assertEquals(config.getRoundCount(), game.getRoundCount());
        assertEquals(0, expTotalBet.compareTo(game.getTotalBets()));
        assertNotNull(game.getTotalWins());

        GameResult result = reportService.calculateGameResult(game);
        assertNotNull(result.getMean());
        assertTrue(result.getMean().compareTo(BigDecimal.ZERO) >= 0);
        assertTrue(result.getVariance().compareTo(BigDecimal.ZERO) >= 0);
        assertTrue(result.getStandardDeviation().compareTo(BigDecimal.ZERO) >= 0);

        log.info("Game result task 1: {}", result);
    }

    @Test
    public void testDiceGame_task2() {
        // Setup
        final DiceTask2Config config = TestData.getDiceConfigTask2();

        // Run the test
        Game game = diceGameService.playDiceGame(config);

        BigDecimal expTotalBet = config.getBetAmount().multiply(BigDecimal.valueOf(config.getRoundCount()));

        // Verify the results
        assertEquals(config.getRoundCount(), game.getRoundCount());
        assertEquals(0, expTotalBet.compareTo(game.getTotalBets()));
        assertNotNull(game.getTotalWins());

        GameResult result = reportService.calculateGameResult(game);
        assertNotNull(result.getMean());
        assertTrue(result.getMean().compareTo(BigDecimal.ZERO) >= 0);
        assertTrue(result.getVariance().compareTo(BigDecimal.ZERO) >= 0);
        assertTrue(result.getStandardDeviation().compareTo(BigDecimal.ZERO) >= 0);

        log.info("Game result task 2: {}", result);
    }

}
