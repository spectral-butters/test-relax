package com.relax.config;

import com.relax.entity.Game;
import com.relax.entity.Round;
import com.relax.entity.slot.SymbolType;

import java.math.BigDecimal;
import java.util.List;

import static com.relax.entity.slot.SymbolType.*;

public class TestData {

    public static Round getWinRound(Game game) {
        Round round = new Round(game);
        round.setBet(BigDecimal.valueOf(1));
        round.setWin(BigDecimal.valueOf(1));
        return round;
    }

    public static Round getLossRound(Game game) {
        Round round = new Round(game);
        round.setBet(BigDecimal.valueOf(1));
        round.setWin(BigDecimal.valueOf(0));
        return round;
    }

    public static DiceTask1Config getDiceConfigTask1() {
        DiceTask1Config config = new DiceTask1Config();
        config.setDiceAmount(1);
        config.setBetAmount(BigDecimal.valueOf(1));
        config.setWinAmount(BigDecimal.valueOf(1));
        config.setTarget(6);
        config.setRoundCount(1000000);
        config.setRollsAmount(4);
        return config;
    }

    public static DiceTask2Config getDiceConfigTask2() {
        DiceTask2Config config = new DiceTask2Config();
        config.setDiceAmount(2);
        config.setBetAmount(BigDecimal.valueOf(1));
        config.setWinAmount(BigDecimal.valueOf(1));
        config.setTarget(6);
        config.setRoundCount(1000000);
        config.setRollsAmount(24);
        return config;
    }

    public static Slot1Config getSlotConfig() {
        Slot1Config config = new Slot1Config();
        SlotConfig.ReelConfig reelConfig = new SlotConfig.ReelConfig();
        List<SymbolType> reel0 = List.of(L1, L2, L2);
        List<SymbolType> reel1 = List.of(L2, L2, L3);
        List<SymbolType> reel2 = List.of(L3, L2, L1);
        reelConfig.setReels(List.of(reel0, reel1, reel2));

        config.setReelConfig(reelConfig);

        config.setBetAmount(BigDecimal.valueOf(10));
        config.setRoundCount(100);
        config.setRows(3);
        config.setColumns(3);
        config.setPaylines(List.of(List.of("10", "11", "12")));

        return config;
    }
}
