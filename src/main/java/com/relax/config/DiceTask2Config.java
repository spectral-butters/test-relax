package com.relax.config;

import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Data
@Component
@ConfigurationProperties(prefix = "dice2")
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class DiceTask2Config implements DiceConfig {

    int roundCount;
    int diceAmount;
    int rollsAmount;
    int target;
    BigDecimal betAmount;
    BigDecimal winAmount;

}
