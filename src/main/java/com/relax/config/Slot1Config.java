package com.relax.config;

import com.relax.entity.slot.Coin;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "slot1")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Slot1Config implements SlotConfig {

    int rows;
    int roundCount;
    int columns;
    int bonusTrigger;
    List<List<String>> paylines;
    List<Coin> coins;
    ReelConfig reelConfig;
    BigDecimal betAmount;

}
