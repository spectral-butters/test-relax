package com.relax.config;

import com.relax.entity.slot.Coin;
import com.relax.entity.slot.SymbolType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

public interface SlotConfig {

    int getRows();

    int getRoundCount();

    int getColumns();

    ReelConfig getReelConfig();

    BigDecimal getBetAmount();

    @Data
    class ReelConfig {
        List<List<SymbolType>> reels;
    }

    List<List<String>> getPaylines();

    List<Coin> getCoins();

    int getBonusTrigger();
}
