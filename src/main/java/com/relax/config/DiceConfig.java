package com.relax.config;

import java.math.BigDecimal;

public interface DiceConfig {

        int getRoundCount();

        int getDiceAmount();

        int getRollsAmount();

        int getTarget();

        BigDecimal getBetAmount();

        BigDecimal getWinAmount();

}
