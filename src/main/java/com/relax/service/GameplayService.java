package com.relax.service;

import com.relax.entity.Game;
import com.relax.entity.report.GameResult;
import com.relax.entity.slot.SlotGame;
import com.relax.config.DiceConfig;
import com.relax.config.DiceTask1Config;
import com.relax.config.DiceTask2Config;
import com.relax.config.Slot1Config;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Data
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class GameplayService {

    DiceGameService diceGameService;
    SlotGameService slotGameService;
    DiceTask1Config config1;
    DiceTask2Config config2;
    Slot1Config slot1Config;
    ReportService reportService;

    @PostConstruct
    public void test() {
        playDiceGame(config1);
        playDiceGame(config2);
        playSlotGame();
    }

    public GameResult playDiceGame(DiceConfig config) {
        log.info("______ DICE GAME ______");
        log.info("Starting Dice Game with config: {}", config);
        Game game = diceGameService.playDiceGame(config);
        GameResult result = reportService.calculateGameResult(game);
        log.info("Game result: {}", result);
        return result;
    }

    public void playSlotGame() {
        log.info("______ SLOT GAME ______");
        log.info("Starting Slot Game with config: {}", slot1Config);
        SlotGame slotGame = slotGameService.playSlotGame(slot1Config);
        log.info("Slot Game result rtp: {}", reportService.calculateRtp(slotGame));
        log.info("Slot Game result: {}", reportService.calculateGameResult(slotGame));
        log.info("Selected coins: {}", slotGame.getSelectedCoins());
    }

}
