package com.relax.service;

import com.relax.entity.Game;
import com.relax.entity.Round;
import com.relax.entity.report.GameResult;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class ReportService {

    /**
     * Calculate the mean of the game's wins.
     * sqrt((sum(powX) / Y)
     * X - wins
     * Y - total rounds
     */
    public BigDecimal calculateMean(Game game) {
        int roundCount = game.getRoundCount();

        if (roundCount == 0) {
            return BigDecimal.ZERO;
        }

        BigDecimal roundCountBD = BigDecimal.valueOf(roundCount);

        BigDecimal sumOfSquares = game.getRounds().values().stream()
                .filter(round -> round != null && round.getWin() != null)
                .map(Round::getWin)
                .map(win -> win.pow(2))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal mean = sumOfSquares.divide(roundCountBD, 2, RoundingMode.HALF_UP)
                .sqrt(new MathContext(4))
                .setScale(2, RoundingMode.HALF_UP);

        return mean;
    }

    /**
     * Calculate the arithmetical mean of the game's wins.
     * sum(X) / Y
     * X - wins
     * Y - total rounds
     * @param game
     * @return
     */
    public BigDecimal calculateArithmeticalMean(Game game) {
        int roundCount = game.getRoundCount();

        if (roundCount == 0) {
            return BigDecimal.ZERO;
        }

        BigDecimal roundCountBD = BigDecimal.valueOf(roundCount);
        return game.getTotalWins().divide(roundCountBD, 2, RoundingMode.HALF_UP);
    }

    /**
     * Calculate the standard deviation of the game's wins.
     * sqrt((sum(pow(X - mean)) / Y)
     * X - wins
     * Y - total rounds
     * mean - arithmetical mean
     * @param game
     * @return
     */
    public BigDecimal calculateStandardDeviation(Game game) {
        BigDecimal mean = calculateArithmeticalMean(game);
        return calculateStandardDeviation(game, mean);
    }

    public BigDecimal calculateStandardDeviation(Game game, BigDecimal mean) {
        if (mean.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }

        BigDecimal roundCountBD = BigDecimal.valueOf(game.getRoundCount());

        BigDecimal sumOfSquaredDifferences = game.getRounds().values().stream()
                .filter(round -> round != null && round.getWin() != null)
                .map(Round::getWin)
                .map(win -> win.subtract(mean).pow(2))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return sumOfSquaredDifferences.divide(roundCountBD, RoundingMode.HALF_UP)
                .sqrt(new MathContext(4))
                .setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Calculate the variance of the game's wins.
     * pow(standardDeviation, 2)
     * @param game
     * @return
     */
    public BigDecimal calculateVariance(Game game) {
        return calculateStandardDeviation(game).pow(2)
                .setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal calculateVariance(BigDecimal standardDeviation) {
        return standardDeviation.pow(2)
                .setScale(2, RoundingMode.HALF_UP);
    }

    public GameResult calculateGameResult(Game game) {
        GameResult result = new GameResult();
        result.setTotalBets(game.getTotalBets());
        result.setTotalWins(game.getTotalWins());
        result.setMean(calculateArithmeticalMean(game));
        result.setStandardDeviation(calculateStandardDeviation(game));
        result.setVariance(calculateVariance(result.getStandardDeviation()));
        result.setRtp(calculateRtp(game));
        return result;
    }

    public BigDecimal calculateRtp(Game game) {
        BigDecimal totalWin = game.getTotalWins();
        BigDecimal totalBet = game.getTotalBets();
        return totalWin.divide(totalBet, 2, RoundingMode.HALF_UP);
    }
}
