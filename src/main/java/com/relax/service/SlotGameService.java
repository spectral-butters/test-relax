package com.relax.service;

import com.relax.entity.Dice;
import com.relax.entity.Round;
import com.relax.entity.slot.*;
import com.relax.config.SlotConfig;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.relax.entity.slot.SymbolType.B1;
import static com.relax.entity.slot.SymbolType.W1;

@Slf4j
@Service
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class SlotGameService {

    /**
     * Main function to play slot game with given config
     * @param config - rules of the game
     * @return - game object with all rounds played
     */
    public SlotGame playSlotGame(SlotConfig config) {
        SlotGame game = new SlotGame(config);

        // Initialize game with reels and paylines
        game.initialize(config);

        int roundCount = config.getRoundCount();

        // Play `roundCount` number of rounds
        for (int i = 0; i < roundCount; i++) {
            Round round = new Round(game);
            round.setBet(config.getBetAmount());
            // Spin the wheel :)
            Symbol[][] symbols = spin(game);

            // Check paylines for winning
            List<SymbolType> win = checkPaylines(game, symbols);
            // Calculate win amount
            round.setWin(calculateWin(win));
            if (win.contains(B1) || triggerBonus(symbols, game.getBonusTrigger())) {
                BigDecimal bonusWin = bonusRound(game, config.getBetAmount());
                round.setWin(round.getWin().add(bonusWin));
            }

            game.getRounds().put(round.getId(), round);
        }

        return game;
    }

    /**
     * Determine if bonus round should be triggered
     * As soon as we have `bonusTrigger` number of bonus symbols, we trigger bonus round
     * @param symbols
     * @param bonusTrigger
     * @return
     */
    public boolean triggerBonus(Symbol[][] symbols, int bonusTrigger) {
        if (bonusTrigger < 1)
            return false;

        int count = 0;
        for (Symbol[] symbolRow : symbols) {
            for (Symbol symbol : symbolRow) {
                if (symbol.getType() == SymbolType.B1) {
                    count++;
                    if (count == bonusTrigger) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Function to check paylines and return sum of winning symbols
     *
     * @param symbolTypes list of symbols
     * @return sum of winning symbols
     */
    public BigDecimal calculateWin(List<SymbolType> symbolTypes) {
        if (symbolTypes.isEmpty()) {
            return BigDecimal.ZERO;
        }

        return symbolTypes.stream()
                .map(SymbolType::getPayout)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Function to spin reels and return resulting symbols
     *
     * @param game slot game with config
     * @return matrix of symbols
     */
    public Symbol[][] spin(SlotGame game) {
        int rows = game.getConfig().getRows();
        int columns = game.getConfig().getColumns();
        int middle = (rows - 1) / 2;

        // Initialize board after spin
        Symbol[][] symbols = new Symbol[rows][columns];
        for (int i = 0; i < game.getReels().size(); i++) {
            Reel reel = game.getReels().get(i);
            Symbol symbol = reel.spin();
            symbols[middle][i] = symbol;

            // Fill in top and bottom rows
            for (int j = 1; j <= middle; j++) {
                int prevIndex = middle - j;
                int nextIndex = middle + j;

                symbols[prevIndex][i] = reel.getPrevious(symbol.getNum(), j);
                symbols[nextIndex][i] = reel.getNext(symbol.getNum(), j);
            }
        }

        return symbols;
    }

    public List<SymbolType> checkPaylines(SlotGame game, Symbol[][] symbols) {
        List<SymbolType> winSymbols = new ArrayList<>();
        for (Payline payline : game.getPaylines()) {
            boolean win = true;
            Position initialPosition = payline.getPositions().get(0);
            Symbol initialSymbol = symbols[initialPosition.getRow()][initialPosition.getColumn()];
            SymbolType winSymbolType = initialSymbol.getType();

            for (int i = 1; i < payline.getPositions().size(); i++) {
                Position position = payline.getPositions().get(i);
                Symbol symbol = symbols[position.getRow()][position.getColumn()];

                if (winSymbolType != symbol.getType()) {
                    // Bonus is irreplaceable, break the loop
                    if (winSymbolType == B1 || symbol.getType() == B1) {
                        win = false;
                        break;
                    }
                    // W1 can replace any symbol except B1
                    if (symbol.getType() == W1) {
                        continue;
                    } else if (winSymbolType == W1) {
                        // Replace winning symbol with non-wild symbol
                        winSymbolType = symbol.getType();
                    } else {
                        win = false;
                        break;
                    }
                }
            }
            if (win) {
                winSymbols.add(winSymbolType);
            }
        }
        return winSymbols;
    }

    /**
     * Function to play bonus round
     *
     * @param game current game
     * @param bet  bet amount
     * @return win amount
     */
    public BigDecimal bonusRound(SlotGame game, BigDecimal bet) {
        Coin coin = selectCoin(game);

        if (game.getSelectedCoins().get(coin.getValue()) == null) {
            game.getSelectedCoins().put(coin.getValue(), 1);
        } else {
            int currentValue = game.getSelectedCoins().get(coin.getValue());
            game.getSelectedCoins().put(coin.getValue(), ++currentValue);
        }

        int winMultiplier = new Dice().roll();

        // Calculate win amount
        // win = coin value * win multiplier * bet amount

        return coin.getValue()
                .multiply(BigDecimal.valueOf(winMultiplier))
                .multiply(bet)
                .setScale(2, RoundingMode.HALF_DOWN);
    }

    /**
     * Function to select a coin from the list of coins based on their weights
     * @param game
     * @return selected coin
     */
    public Coin selectCoin(SlotGame game) {
        int totalWeight = game.getCoins()
                .stream().mapToInt(Coin::getWeight).sum();

        // Generate a random number between 0 and the total weight
        int randomWeight = new Random().nextInt(totalWeight);

        // Iterate over the coins and subtract their weights from the random weight
        for (Coin coin : game.getCoins()) {
            randomWeight -= coin.getWeight();
            if (randomWeight < 0) {
                // Return the coin when the random weight becomes negative or zero
                return coin;
            }
        }

        throw new IllegalArgumentException("Invalid coin weights");
    }
}
