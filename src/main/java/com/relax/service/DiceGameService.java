package com.relax.service;

import com.relax.entity.Dice;
import com.relax.entity.dice.DiceGame;
import com.relax.entity.Game;
import com.relax.entity.Round;
import com.relax.config.DiceConfig;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class DiceGameService {

    /**
     * Main method for playing a dice game
     * @param config - configuration for the game
     * @return - Game result with played rounds
     */
    public Game playDiceGame(DiceConfig config) {
        Dice dice = new Dice();
        DiceGame game = new DiceGame(config);

        for (int i = 0; i < game.getConfig().getRoundCount(); i++) {
            Round round = new Round(game);
            round.setBet(game.getConfig().getBetAmount());

            // Play `rollsAmount` number of rolls
            for (int j = 0; j < game.getConfig().getRollsAmount(); j++) {
                boolean win = true;

                // Roll `diceAmount` number of dices
                for (int k = 0; k < game.getConfig().getDiceAmount(); k++) {
                    int roll = dice.roll();

                    // If any dice number is not equal to the target, the round is lost
                    if (roll != game.getConfig().getTarget()) {
                        win = false;
                        break;
                    }
                }
                // If all dice numbers are equal to the target, the round is won
                if (win) {
                    round.setWin(game.getConfig().getWinAmount());
                    break;
                }
            }
            game.getRounds().put(round.getId(), round);
        }

        return game;
    }

}
