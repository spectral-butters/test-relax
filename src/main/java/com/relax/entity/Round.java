package com.relax.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Round {

    @EqualsAndHashCode.Include
    String id;
    BigDecimal bet;
    BigDecimal win;

    // Generate random id on instantiation
    public Round(Game game) {
        this.id = genId();
        while(game.getRounds().containsKey(this.id)) {
            this.id = genId();
        }
    }

    private String genId() {
        return UUID.randomUUID()
                .toString()
                .replace("-", "")
                .substring(0, 8);
    }

}
