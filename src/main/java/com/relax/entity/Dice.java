package com.relax.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Random;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Dice {

    Random random = new Random();

    public int roll() {
        return random.nextInt(6) + 1;
    }

}
