package com.relax.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class AbstractGame implements Game {

    @EqualsAndHashCode.Include
    String id;
    Map<String, Round> rounds = new HashMap<>();
    public int getRoundCount() {
        return rounds.size();
    }

    public BigDecimal getTotalBets() {
        return rounds.values()
                .stream()
                .filter(round -> round != null && round.getBet() != null)
                .map(Round::getBet)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getTotalWins() {
        return rounds.values()
                .stream()
                .filter(round -> round != null && round.getWin() != null)
                .map(Round::getWin)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    protected String generateId() {
        return java.util.UUID.randomUUID()
                .toString()
                .replace("-", "")
                .substring(0, 8);
    }
}
