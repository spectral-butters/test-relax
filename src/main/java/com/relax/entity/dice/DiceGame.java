package com.relax.entity.dice;

import com.relax.entity.AbstractGame;
import com.relax.config.DiceConfig;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class DiceGame extends AbstractGame {

    DiceConfig config;

    // Generate random id on instantiation
    public DiceGame(DiceConfig config) {
        this.setId(generateId());
        this.config = config;
    }

}
