package com.relax.entity.report;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class GameResult {

    BigDecimal totalBets;
    BigDecimal totalWins;
    BigDecimal mean;
    BigDecimal variance;
    BigDecimal standardDeviation;
    BigDecimal rtp;

}
