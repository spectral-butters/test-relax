package com.relax.entity.slot;

import com.relax.entity.AbstractGame;
import com.relax.config.SlotConfig;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SlotGame extends AbstractGame {

    SlotConfig config;
    List<Reel> reels = new ArrayList<>();
    List<Payline> paylines = new ArrayList<>();
    List<Coin> coins = new ArrayList<>();
    int bonusTrigger;

    // Keep track of selected coins for reporting purposes
    Map<BigDecimal, Integer> selectedCoins = new HashMap<>();

    public SlotGame(SlotConfig config) {
        this.setId(generateId());
        this.config = config;
        this.bonusTrigger = config.getBonusTrigger();
    }

    public void initialize(SlotConfig config) {
        if (config.getRows() % 2 == 0 || config.getRows() != config.getColumns()) {
            // Only odd rows and columns are supported
            throw new IllegalArgumentException("Rows and columns must be odd and equal");
        }

        // Validate reels
        if (config.getReelConfig().getReels().size() != config.getColumns()) {
            throw new IllegalArgumentException("Number of reels must be equal to number of columns");
        }

        // Initialize reels from config
        for (int i = 0; i < config.getReelConfig().getReels().size(); i++) {
            Reel reel = new Reel(config.getReelConfig().getReels().get(i));
            reels.add(i, reel);
        }

        // Initialize paylines from config
        for (List<String> paylineConfig: config.getPaylines()) {
            paylines.add(new Payline(paylineConfig));
        }

        // Initialize coins from config
        setCoins(config.getCoins());
    }

}
