package com.relax.entity.slot;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

import static lombok.AccessLevel.PRIVATE;

@Getter
@AllArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public enum SymbolType {
    W1("Wild", new BigDecimal("2000")),
    H1("Seven", new BigDecimal("800")),
    H2("Bell", new BigDecimal("400")),
    H3("Bar", new BigDecimal("80")),
    L1("Banana", new BigDecimal("60")),
    L2("Orange", new BigDecimal("20")),
    L3("Plum", new BigDecimal("16")),
    L4("Cherry", new BigDecimal("12")),
    B1("Bonus", BigDecimal.ZERO);

    String name;
    BigDecimal payout;
}
