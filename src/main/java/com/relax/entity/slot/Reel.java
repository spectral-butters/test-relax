package com.relax.entity.slot;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Reel {

    List<Symbol> symbols;
    Random random = new Random();

    public Reel(List<SymbolType> symbolList) {
        symbols = new ArrayList<>(symbolList.size());
        // Initialize reel with predefined symbols
        for (int i = 0; i < symbolList.size(); i++) {
            Symbol symbol = new Symbol();
            symbol.setNum(i);
            symbol.setType(symbolList.get(i));
            symbols.add(i, symbol);
        }
    }

    public Symbol spin() {
        return symbols.get(random.nextInt(symbols.size()));
    }

    public Symbol getPrevious(int current, int step) {
        return symbols.get((current + symbols.size() - step) % symbols.size());
    }

    public Symbol getNext(int current, int step) {
        return symbols.get((current + step) % symbols.size());
    }

}
