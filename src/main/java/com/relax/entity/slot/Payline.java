package com.relax.entity.slot;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Payline {

    List<Position> positions = new ArrayList<>();

    public Payline(List<String> payLineConfig) {
        for (String positionString : payLineConfig) {
            if (positionString.length() != 2) {
                throw new IllegalArgumentException("Invalid payline configuration: " + positionString);
            }
            int row = Character.getNumericValue(positionString.charAt(0));
            int column = Character.getNumericValue(positionString.charAt(1));
            positions.add(new Position(row, column));
        }
    }
}
