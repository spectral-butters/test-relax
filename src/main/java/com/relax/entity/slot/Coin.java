package com.relax.entity.slot;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Coin {

    BigDecimal value;
    int weight;

}
