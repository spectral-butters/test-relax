package com.relax.entity;

import java.math.BigDecimal;
import java.util.Map;

public interface Game {

    Map<String, Round> getRounds();

    int getRoundCount();

    BigDecimal getTotalBets();

    BigDecimal getTotalWins();

}
